/**************************
  Andrew Jensen
  CPSC 1021, Section 3,F20
  aejense@g.clemson.edu
  Elliot McMillan
**************************/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <cstdlib>

using namespace std;


typedef struct Employee{
	string lastName;
	string firstName;
	int birthYear;
	double hourlyWage;
}employee;

bool name_order(const employee& lhs, const employee& rhs);
int myrandom (int i) { return rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));


  /*Create an array of 10 employees and fill information from standard input with prompt messages*/
	employee person[10];
	for (int i = 0; i < 10; i++) {
		cout << "Enter the first name of employee #" << i+1 << ": ";
		cin >> person[i].firstName;
		cout << "Enter the last name of employee #" << i+1 << ": ";
		cin >> person[i].lastName;
		cout << "Enter the birth year of employee #" << i+1 << ": ";
		cin >> person[i].birthYear;
		cout << "Enter the hourly wage of employee #" << i+1 << ": ";
		cin >> person[i].hourlyWage;
		cout << endl;
	}
  /*After the array is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/
	int array[10] = {0,1,2,3,4,5,6,7,8,9};
	int *ptr = &array[0];
	int *endPtr = &array[10];
	random_shuffle(ptr, endPtr, myrandom);


   /*Build a smaller array of 5 employees from the first five cards of the array created
    *above*/
	employee smallEmp[5];
	for (int i = 0; i < 5; i++) {
		smallEmp[i].lastName = person[array[i]].lastName;
		smallEmp[i].firstName = person[array[i]].firstName;
		smallEmp[i].birthYear = person[array[i]].birthYear;
		smallEmp[i].hourlyWage = person[array[i]].hourlyWage;
	}


    /*Sort the new array.  Links to how to call this function is in the specs
     *provided*/
	int numSwap = 1;
	while (numSwap != 0) {
		numSwap = 0;
		for (int i = 0; i < 4; i++) {
			bool doSwap = name_order(smallEmp[i], smallEmp[i+1]);
			if (doSwap == false) {
				swap(smallEmp[i], smallEmp[i+1]);
				numSwap++;
			}
		}
	}

    /*Now print the array below */
	for (int i = 0; i < 5; i++) {
		cout << setw(20) << right << smallEmp[i].lastName + ", " + smallEmp[i].firstName << endl;
		cout << setw(20) << right << smallEmp[i].birthYear << endl;
		cout.precision(2);
		cout << setw(20) << right << fixed << showpoint << smallEmp[i].hourlyWage << endl;
	}
	
  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool name_order(const employee& lhs, const employee& rhs) {
	if (lhs.lastName[0] < rhs.lastName[0]) return true;
	else return false;
}
